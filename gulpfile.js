/**
 * Gulpfile.js
 *
 * This is the entrypoint for gulp to look for our tasks. Please
 * add your tasks under gulp/tasks/<task>.js.
 */
try {
    require('require-reload')('./gulp/support/project-validator.js');
    require('./gulp');
} catch (error) {
    require('./gulp/support/helper').warning(error + '');
}