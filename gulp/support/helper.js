var util = require('gulp-util'),
    reload = require('require-reload')(require),
    project = reload('../../../project.json'),
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    _helper_cache = {
        _project: {
            _has: {},
            _values: {}
        }
    };

/**
 * Helper()
 *
 * This is the constructor function for the Helper class.
 * It provides helper functionality used throughout our
 * gulp tasks.
 *
 * @constructor
 */
var Helper = function () {
    this.cli = util;

    this.imageOptimizationLevels = {
        low: 1,
        medium: 3,
        high: 5,
        absurd: 7
    };
};

/**
 * Helper.src()
 *
 * A plumbed gulp.src with our custom error logger. Use this instead of
 * gulp.src. The second parameter is true by default and will disable
 * replacing the path aliases from project.json when set to false.
 */
Helper.prototype.src = function (globs, replacePaths) {
    replacePaths = replacePaths ? replacePaths : true;

    // Let's convert the aliases to paths if desired
    if (replacePaths !== false) {
        // We'll cast "globs" to an array if it isn't one already
        if (globs.constructor !== Array) {
            globs = [globs];
        }
        // Then we'll map over the array and replace the
        // paths with their correct value.
        globs = globs.map(function (glob) {
            return this.replacePaths(glob);
        }.bind(this));
    }

    // We plumb the src'ed globs with a custom plumber
    // error handler and return that.
    return gulp.src(globs)
        .pipe(plumber({
            errorHandler: function (message) {
                this.warning(message, true);
            }.bind(this)
        }));
};

/**
 * Helper.reloadProjectFile()
 *
 * Forces a reload of the project.json file. Mostly
 * used internally.
 *
 * @returns {void}
 */
Helper.prototype.reloadProjectFile = function () {
    project = reload('../../../project.json');
};

/**
 * Helper.project.has()
 *
 * @param objectPath
 * @returns {boolean}
 */
Helper.prototype.has = function (objectPath) {
    if (!_helper_cache._project._has.hasOwnProperty(objectPath)) {
        var result = this.read(objectPath);
        _helper_cache._project._has[objectPath] = typeof result !== 'undefined';
    }

    return _helper_cache._project._has[objectPath];
};

/**
 * Helper.config()
 *
 * Easily retreive project configuration
 * settings from project.json with defaults!
 *
 * @param objectPath
 * @param defaultValue
 * @returns {*}
 */
Helper.prototype.get = function (objectPath, defaultValue) {
    if (this.has(objectPath)) {
        return this.read(objectPath);
    } else {
        return defaultValue;
    }
};

/**
 * Helper.project.get()
 *
 * @param objectPath
 * @returns {*}
 */
Helper.prototype.read = function (objectPath) {
    if (!_helper_cache._project._values.hasOwnProperty(objectPath)) {
        var segments = objectPath.split(".");
        this.reloadProjectFile();

        _helper_cache._project._values[objectPath] = segments.reduce(function (rest, currentKey) {
            if (typeof rest !== "undefined" && rest.hasOwnProperty(currentKey)) {
                return rest[currentKey];
            } else {
                return undefined;
            }
        }, project);
    }

    return _helper_cache._project._values[objectPath];
};

/**
 * Helper.dir()
 *
 * Properly builds up paths based on the project configuration.
 *
 * @param projectPathKey {string} The path from project.json you want to build up on
 * @param path {string} The path you wish to append
 * @returns {string} The resulting normalized path
 */
Helper.prototype.dir = function (projectPathKey, path) {
    if (typeof path == "undefined") {
        return this.replacePaths(projectPathKey);
    }

    this.reloadProjectFile();

    var projectPath = this.replacePaths(projectPathKey);
    var projectPathSegments = projectPath.split('/');

    if (path && path != '' && path !== null) {
        var pathSegments = path.split('/');

        for (var index = 0, pathLength = pathSegments.length; index < pathLength; index += 1) {
            projectPathSegments.push(pathSegments[index]);
        }
    }

    return projectPathSegments.join('/');
};

/**
 * Helper.replacePaths()
 *
 * @param path
 * @returns {*}
 */
Helper.prototype.replacePaths = function (path) {
    this.reloadProjectFile();

    var projectPaths = Object.keys(this.get('paths'));

    projectPaths.forEach(function (projectPathKey, index) {
        var projectPath = project.paths[projectPathKey];
        var match = '\{(' + projectPathKey + ')\}';
        var matchExp = new RegExp(match, 'gi');

        if (path.match(matchExp) !== null) {
            path = path.replace(matchExp, projectPath);
        }
    });

    return path;
};

/**
 * Helper.log()
 *
 * Logs a message to the console. This is controlled
 * by the "verbose" : {boolean} setting in project.json.
 *
 * @param message {string}
 * @param important {boolean}
 */
Helper.prototype.log = function (message, important) {
    this.reloadProjectFile();

    important = important || false;
    var verbose = project.hasOwnProperty('verbose') ? project.verbose : false;

    if (verbose || important) {
        util.log(message);
    }
};

/**
 * @param message
 * @param important
 * @returns {*|number}
 */
Helper.prototype.comment = function (message, important) {
    return this.log.call(this, util.colors.gray(message), important);
};

/**
 * @param message
 * @param important
 * @returns {*|number}
 */
Helper.prototype.info = function (message, important) {
    return this.log.call(this, util.colors.cyan(message), important);
};

/**
 * @param message
 * @param important
 * @returns {*|number}
 */
Helper.prototype.warning = function (message, important) {
    return this.log.call(this, util.colors.yellow(message), important);
};

/**
 * @param path
 * @param important
 * @returns {*}
 */
Helper.prototype.watching = function (path, important) {
    return this.log.call(this, util.colors.gray("Now watching " + util.colors.underline(path) + " for changes"), important);
};

/**
 * @param message
 * @param important
 * @returns {*|number}
 */
Helper.prototype.success = function (message, important) {
    this.log.call(this, util.colors.green(message), important);
};

module.exports = new Helper;