var gulp = require('gulp'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    merge = require('merge-stream'),
    helper = require('../support/helper');

/**
 * Task "images"
 */
gulp.task('images', ['images:optimize'], function (callback) {
    return callback();
});

/**
 * Task "images:cleanup"
 *
 * Cleans up the build/img directory by removing
 * it.
 */
gulp.task('images:cleanup', function (callback) {
    helper.info('Cleaning up optimized images ...');

    del([helper.dir('{build}/img')], {
        force: true
    }, function (error, paths) {
        paths.forEach(function (path) {
            helper.comment("Deleted " + path + "!");
        });
        callback();
    });
});

/**
 * Task "images:optimze"
 *
 * Runs the images through imagemin and
 * dumps them out to the build folder
 */
gulp.task('images:optimize', function (callback) {
    if (helper.get('images.optimize') === false) {
        callback();
        return;
    }

    helper.info('Optimizing images ...');

    var optimizationLevel = helper.imageOptimizationLevels[helper.get('images.optimize')];

    var themeImages = helper.src('{theme}/img/**/*.{gif,jpg,jpeg,png,svg}')
        .pipe(imagemin({optimizationLevel: optimizationLevel}))
        .pipe(gulp.dest(helper.dir('{build}/img')));

    var contentImages = helper.src('{content}/**/*.{gif,jpg,jpeg,png,svg}')
        .pipe(imagemin({optimizationLevel: optimizationLevel}))
        .pipe(gulp.dest(helper.dir('{content}')));

    return merge(themeImages, contentImages)
        .on('end', function () {
            helper.success("Finished optimizing images!");
        });
});