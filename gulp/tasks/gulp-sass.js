var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    sass = require('gulp-sass'),
    autoprefix = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    helper = require('../support/helper'),
    combineMediaQueries = require('gulp-combine-media-queries'),
    minify = require('gulp-minify-css');

gulp.task('sass', ['sass:build'], function (callback) {
    callback();
});

gulp.task('sass:build', function () {

    var sassFiles = helper.src(helper.get('css.entrypoint'));

    // We always have to pipe this into the sourcemaps plugin,
    // even when minification is turned on, because gulp-sass
    // or one of its dependencies breaks the SASS compiler
    // otherwise.
    sassFiles = sassFiles.pipe(sourcemaps.init())
        .pipe(sass({
            // Set the default, non-minified output style
            outputStyle: 'compact',
            // For good measure
            includePaths: [helper.dir('{theme}/sass/')],
            // Does this work at all?
            disableWarnings: true
        }))
        .pipe(
            gulpif(
                helper.get('css.autoprefix') === true, 
                autoprefix({
                    cascade: true,
                    browsers: ['> 0.3%']
                })
            )
        );

    if (helper.get('css.minify') == false) {
        helper.comment("Generating sourcemaps ...");
        // We'll only write the sourcemap if minification is turned off.
        // @todo Add reasoning behind this to documentation.
        // @todo Write documentation first.
        sassFiles = sassFiles.pipe(sourcemaps.write());
    } else {
        helper.comment("Minifying CSS after compiling ...");
        // @todo Evaluate whether this is actually needed in combination with gulp-minify-css
        sassFiles = sassFiles.pipe(combineMediaQueries({log: true}))
            .pipe(minify());
    }

    return sassFiles.pipe(gulp.dest(helper.dir('{build}/css')));

});