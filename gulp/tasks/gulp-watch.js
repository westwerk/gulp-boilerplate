var gulp = require('gulp'),
    watch = require('gulp-watch'),
    livereload = require('gulp-livereload'),
    helper = require('../support/helper');

/**
 * Task "watch"
 */
gulp.task('watch', ['watch:js', 'watch:css', 'watch:copy', 'watch:bower', 'watch:js', 'watch:images', 'watch:livereload'], function (callback) {
    callback();
});

/**
 * Task "watch:js"
 */
gulp.task('watch:js', ['js'], function (callback) {
    var paths = [
        helper.dir('{theme}/js/**/*.{js,json}'),
        'package.json'
    ];

    helper.watching(paths.join(','));

    return gulp.watch(paths, ['js']);
});

/**
 * Task "watch:images"
 */
gulp.task('watch:images', ['images'], function (callback) {
    var imagesPath = helper.dir('theme', 'img/**/*.{gif,jpg,jpeg,png,svg}');
    helper.watching(imagesPath);
    gulp.watch([
        imagesPath,
        helper.dir('{root}/../files/content/**/*.{gif,jpg,jpeg,png,svg}')
    ], ['images']);
    callback();
});

/**
 * Task "watch:less"
 *
 * Watches the theme path specified in project.json
 * for changes and triggers the less task.
 */
gulp.task('watch:css', ['css'], function (callback) {
    var preprocessor = helper.get('css.preprocessor');
    var sourceTypes = preprocessor;

    if (preprocessor == 'sass') {
        sourceTypes = '{sass,scss}';
    }

    gulp.watch(helper.dir('{theme}/' + sourceTypes + '/**/*.' + sourceTypes), [preprocessor]);

    helper.watching(helper.dir('{theme}/' + sourceTypes + '/**/*.' + sourceTypes));

    return callback();
});

/**
 * Task "watch:livereload"
 *
 * Watches the configured livereload endpoints
 * and triggers the changed method when something
 * happens to them.
 */
gulp.task('watch:livereload', function () {
    if (!helper.has('livereload') ||
        helper.get('livereload').length < 1) {
        helper.comment('No expressions for livereload to watch ...');
        return;
    }

    helper.info("Starting livereload server ...");
    livereload.listen({
        quiet: !helper.get('verbose')
    });

    helper.get('livereload').forEach(function (expression) {
        expression = helper.replacePaths(expression);
        helper.comment("Now watching " + expression + " for changes to livereload.");

        gulp.watch(expression)
            .on('change', function (object) {
                livereload.changed(object);
            });
    });
});

/**
 * Task "watch:copy"
 */
gulp.task('watch:copy', function () {
    // Check if we have copy tasks configured
    if (!helper.has('copy')) {
        helper.comment('Nothing to copy.');
        return;
    }

    var paths = helper.get('copy');
    var globs = Object.getOwnPropertyNames(paths)
        .map(function(glob) {
        return helper.replacePaths(glob);
    });

    return gulp.watch(globs, ['copy']);
});

/**
 * Task "watch:bower"
 *
 * Gather every configured bower component files
 * and "publish" them to the build directory
 */
gulp.task('watch:bower', ['bower'], function (callback) {
    // Check if we have bower components configured
    if (!helper.has('bower')) {
        helper.comment('No bower packages configured.');
        return;
    }

    // Start out with the project.json file
    var globbingExpressions = ['project.json'];

    // Let's iterate over each configured component
    Object.keys(helper.get('bower')).forEach(function (componentName) {
        if (!helper.has('bower.' + componentName)) {
            helper.comment('Inaccessible bower component configuration (' + componentName + ').');
            return;
        }

        // Get the copy operations defined in each component
        var componentCopyOps = helper.get('bower.' + componentName);
        componentCopyOps.forEach(function (copyOp) {
            // Normalize the paths, replacing every {pathKey} with
            // the proper path as defined in "project.paths"
            var expression = helper.dir('{bower}/' + componentName + '/' + copyOp[0]);

            // Display a verbose watching statement
            helper.watching(expression);

            // Build our glob stack with expressions to watch
            globbingExpressions.push(expression);
        });
    });

    // Start the watcher for the globbing expressions stack
    gulp.watch(globbingExpressions, ['bower']);

    callback();
});