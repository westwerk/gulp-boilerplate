var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    autoprefix = require('gulp-autoprefixer'),
    less = require('gulp-less'),
    lessSourceMap = require('gulp-less-sourcemap'),
    lessPluginCleanCss = require('less-plugin-clean-css'),
    helper = require('../support/helper');

/**
 * Task "less"
 */
gulp.task('less', ['less:build'], function (callback) {
    return callback();
});

/**
 * Task "less:build"
 *
 * Builds the main.css file from main.less after
 * running the cleanup task.
 */
gulp.task('less:build', function () {
    helper.info('Building CSS from LESS sources ...');

    var lessCleanCss = new lessPluginCleanCss({
        advanced: true
    });

    var files = helper.src(helper.get('css.entrypoint'));

    files = files.on('error', function (error) {
        helper.error(error);
    });

    var lessPlugins = [];

    if (helper.get('css.minify') === true) {
        helper.comment("Minifying CSS sources after compiling ...");

        lessPlugins.push(lessCleanCss);
    }

    if (helper.get('css.sourcemap') === true) {
        helper.comment("Building CSS with a source map ...");

        files = files.pipe(lessSourceMap({
            plugins: lessPlugins,
            sourceMap: {
                sourceMapBasepath: helper.dir('{root}'),
                sourceMapRootpath: helper.dir('{theme}/less')
            }
        }));
    } else {
        helper.comment("Building CSS without a source map ...");

        files = files.pipe(less({
            plugins: lessPlugins
        }));
    }

    return files
        .pipe(gulpif(
            helper.get('css.sourcemap') === false,
            autoprefix({
                cascade: true,
                browsers: 'last 3 versions, Safari >= 8, iOS >= 7'
            })
        ))
        .pipe(gulp.dest(helper.dir('{build}/css')))
        .on('end', function () {
            helper.success("Finished compiling LESS sources!");
        });
});