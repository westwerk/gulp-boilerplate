var gulp = require('gulp'),
    del = require('del'),
    helper = require('../support/helper'),
    jsTasks = [];

switch(helper.get('js.loader')) {

    // The simple loader expects a "js.bundle" entry in project.json
    // which should contain all files you want to concatenate and
    // optionally minify.
    case "simple":
        jsTasks.push('scripts');
        break;

    // The browserify loader still needs some work, and then some
    // documentation.
    case "browserify":
        jsTasks.push('browserify');
        break;

    // Whoa, whoa, whoa. Hold your horses there.
    default:
        throw new Error("Unknow script loader: " + helper.get('css.preprocessor'));
        break;
}

/**
 * Task "less"
 */
gulp.task('js', jsTasks, function (callback) {
    callback();
});

/**
 * Task "js:cleanup"
 *
 * Cleans up the build/js directory by removing its contents.
 */
gulp.task('js:cleanup', function (callback) {
    helper.info('Cleaning up compiled script files ...');

    del([helper.dir('{build}/js/*')], {
        force: true
    }, function(error, paths) {
        if(!paths) {
            callback();
            return;
        }

        paths.forEach(function(path) {
            helper.comment("Deleted " + path + "!");
        });

        callback();
    });
});