var gulp = require('gulp'),
    tap = require('gulp-tap'),
    helper = require('../support/helper');

/**
 * Task "bower"
 */
gulp.task('bower', ['bower:build'], function (callback) {
    callback();
});

/**
 * Task "bower:cleanup"
 */
gulp.task('bower:cleanup', function (callback) {
    helper.info('Cleaning up Bower component assets ...');
    callback();
});

/**
 * Task "bower:build"
 */
gulp.task('bower:build', ['bower:cleanup'], function (callback) {
    helper.info('Publishing Bower component assets ...');

    // Check if we have bower components configured
    if (!helper.has('bower')) {
        helper.comment('No bower packages configured.');
        return;
    }

    // Let's iterate over each configured component
    Object.keys(helper.get('bower')).forEach(function (componentName, index, componentNames) {
        if (!helper.has('bower.' + componentName)) {
            helper.comment('Inaccessible bower component configuration (' + componentName + ').');
            return;
        }

        // Get the copy operations defined in each component
        var componentCopyOps = helper.get('bower.' + componentName);
        componentCopyOps.forEach(function (copyOp, index) {

            // ensure a "from" globbing expression as well as
            // a "to" destination is set
            if (copyOp.length != 2) {
                helper.warning('Invalid copy operation for "' + componentName + '" (entry #' + (index + 1) + ')!', true);
                return;
            }

            // Normalize the paths, replacing every {pathKey} with
            // the proper path as defined in "project.paths"
            var from = helper.dir('{bower}/' + componentName + '/' + copyOp[0]);
            var to = helper.dir(copyOp[1]);

            // Apply the copy operation
            // @todo find a way to keep track of these to build a cleanup task later
            helper.src([from], false)
                .pipe(tap(function (file) {
                    helper.comment("Publishing " + file.path + " ...");
                    return file;
                }))
                .pipe(gulp.dest(to));
        }, this);
    });

    return callback();
});