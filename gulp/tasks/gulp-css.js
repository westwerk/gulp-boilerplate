var gulp = require('gulp'),
    del = require('del'),
    helper = require('../support/helper'),
    cssTasks = [];

switch(helper.get('css.preprocessor')) {
    case "less":
        cssTasks.push('less:build');
        break;

    case "sass":
        cssTasks.push('sass:build');
        break;

    default:
        throw new Error("Unknow CSS preprocessor: " + helper.get('css.preprocessor'));
        break;
}

/**
 * Task "less"
 */
gulp.task('css', cssTasks, function (callback) {
    return callback();
});

/**
 * Task "css:cleanup"
 *
 * Cleans up the build/css directory by removing it.
 */
gulp.task('css:cleanup', function (callback) {
    helper.info('Cleaning up compiled CSS files ...');
    del([helper.dir('{build}/css/*')], {
        force: true
    }, function(error, paths) {
        paths.forEach(function(path) {
            helper.comment("Deleted " + path + "!");
        });
        callback();
    });
});