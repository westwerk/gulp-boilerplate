var gulp = require('gulp');

/**
 * Task "default"
 *
 * Default gulp task. Executed when running "gulp" with no
 * task list supplied.
 *
 * - copy: Simple copy task which reads globbing expressions from project.json
 * - js: Dynamic task switch, runs the configured JS loader
 * - css: Dynamic task switch, runs the configured CSS preprocessor
 * - images: Copies theme and content images and then optimizes them
 * - bower: Takes care of all your Bower package asset publishing
 * - watch: Starts several watchers to keep an eye on those files of y'all
 *
 */
gulp.task('default', ['copy', 'js', 'css', 'images', 'bower', 'watch']);

/**
 * Task "dev"
 *
 * @alias to the default task.
 */
gulp.task('dev', ['default']);

/**
 * Task "dist"
 *
 * @alias to the archive task.
 */
gulp.task('dist', ['archive:build']);