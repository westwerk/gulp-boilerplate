var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    helper = require('../support/helper');

gulp.task('scripts', ['scripts:bundle'], function (callback) {
    callback();
});

/**
 * Task "scripts:bundle"
 *
 * This is just a simple task to concatenate and optionally
 * minify all files listed under "js.bundle" in project.json
 */
gulp.task('scripts:bundle', function (callback) {
    var bundle = helper.get('js.bundle');

    if (bundle.length < 1) {
        callback();
        return;
    }

    var scriptFiles = helper.src(bundle)
        .pipe(concat('main.js'));

    if (helper.get('js.minify') == true) {
        helper.comment('Minifying bundled script file ...');
        scriptFiles.pipe(uglify());
    }

    return scriptFiles.pipe(gulp.dest(helper.dir('{build}/js')));
});